
window.onload = (event) => {
    // check if user has a token, if it does, redirect to homepage for authentication and authorization, if not then continue
    verifyToken()
    // check if the needed data for this page is stored in cookies, if not, then retrieve

    // page manipulation
}


// check if there is token of auth, if there is, redirect to homepage
function verifyToken() {
    var token = localStorage.getItem('law-token');
    if (token) {
        window.location.href = 'homepage';
    } else {
        // do nothing
    }
}

function wrongCredentials() {
    document.getElementById("errorLogin").classList.remove("d-none");
}

// function to submit login form to auth service
function submitLogin() {
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    let payload = {
        "email": email,
        "password": password
    };
    fetch("https://law-auth-service.herokuapp.com/signin", {
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(payload)
    }).then(response=>response.json())
    .then(data=>{
        // if login attempt is a success, we receive a token, 
        if(data.token){
            localStorage.setItem("law-token", data.token);
            // after login in, "redirect" user to its home page
            window.location.href = 'homepage';
        // if not,
        } else {
            wrongCredentials();
        }
    });
}


window.onload = (event) => {
    // check if user is authenticated and authorized, if not redirect to login page
    verify()

    // check if the needed data for this page is stored in cookies, if not, then retrieve
    
    // page manipulation
}

async function verify() {
    var token = localStorage.getItem('law-token');
    if(token) {
        return fetch("https://law-auth-service.herokuapp.com/verify", {
            method: "GET",
            headers: {'Content-Type': 'application/json', "Token": token}
        }).then(response=>response.json())
        .then(data=>{ 
            if(data.role == "dokter") {
                dokterSetup(data)
            } else if(data.role == "pasien") {
                pasienSetup(data)
            } else {
                localStorage.removeItem('law-token');
                window.location.href = 'login';
            }
        });
    } else {
        localStorage.removeItem('law-token');
        window.location.href = 'login';
    }
}

function dokterSetup(data) {
    dokterHomepage()
    var sumAntrian = getTotalAntrianPasienSaatIni()

    document.getElementById("greeting").innerHTML = "Selamat Datang, " + data.name
    document.getElementById("numAntrian").innerHTML = "Total Antrian Pasien Saat Ini:" + sumAntrian

}

function getTotalAntrianPasienSaatIni() {
    // TODO: implement fetch ke service, sementara mock up 15
    return 15;
}

function pasienSetup(data) {
    pasienHomepage()
    // TODO: fetch info tiket antrian yang sedang berjalan
    document.getElementById("greeting").innerHTML = "Selamat Datang, " + data.name
}

function logout() {
    localStorage.removeItem("law-token");
    window.location.href = 'login';
}

// ---------------- Page Manipulation Functions ----------------
function dokterHomepage() {
    // TODO: ganti href buttons ke halaman web service
    document.getElementById('navbar_buttons').innerHTML =`
    <li class="nav-item"><a class="nav-link" href="#">Daftar Konsultasi</a></li>
    <li class="nav-item"><a class="nav-link" href="/list_order">Daftar Pemesanan</a></li>
    <li class="nav-item"><a class="nav-link" href="#">Profil</a></li>
    <li class="nav-item"><a class="nav-link" href="#" onclick="logout();">Logout</a></li>
    `
    // TODO: ganti href buttons ke halaman web service
    document.getElementById('content').innerHTML =`
    <div class="text-center mt-5">
        <h1 id=greeting></h1>
        <h2 id=numAntrian></h2>
    </div>
    <div class="row row-cols-lg-3 g-2 g-lg-3 p-3">
    <button type="button" class="btn btn-primary p-3 border" href="#">Terima Order</button>
    <button type="button" class="btn btn-primary p-3 border" href="/list_order">Riwayat Order</button>
    <button type="button" class="btn btn-primary p-3 border" href="#">Chat</button>
    </div>
    `
}

function pasienHomepage() {
    // TODO: ganti href buttons ke halaman web service
    document.getElementById('navbar_buttons').innerHTML =`
    <li class="nav-item"><a class="nav-link" href="#">Pemesanan</a></li>
    <li class="nav-item"><a class="nav-link" href="#">Antrian</a></li>
    <li class="nav-item"><a class="nav-link" href="#">Profil</a></li>
    <li class="nav-item"><a class="nav-link" href="#" onclick="logout();">Logout</a></li>
    `
    // TODO: ganti href buttons ke halaman web service
    document.getElementById('content').innerHTML =`
    <div class="text-center mt-5">
        <h1 id="greeting"></h1>
    </div>
    <div id="currAntrian" class="card">
        <div class="row">
            <div class="col-sm">
                <h2>No Antrian: 15</h2>
            </div>
            <div class="col-sm">
                <h3>Tanggal: 30/4/2022</h3>
                <h3>Status: Belum diterima</h3>
            </div>
            <div class="col-sm">
                <h3>Dokter: -</h3>
            </div>
        </div>
    </div>
    </div>
    <div class="row row-cols-lg-3 g-2 g-lg-3 p-3">
    <button type="button" class="btn btn-primary p-3 border" href="#">Ambil Tiket Antrian</button>
    <button type="button" class="btn btn-primary p-3 border" href="#">Daftar Pesanan</button>
    <button type="button" class="btn btn-primary p-3 border" href="#">Bantuan Teknis</button>
    </div>
    `
}
// -------------------------------------------------------------